# Автор: Igor A. Shmakov
# Первая версия: 2021-XX-XX
# Версия от: 2021-11-13
# № версии: 0.05
# Лицензия: GPLv3

import requests


def sendMessageToTelegramBot(bot_message):
    """
    Модуль для отправки сообщения в Telegram.

        Parameters:
            bot_message (str): отправляемая строка.

        Returns:
            response.json(): ответ json

    FIXME: XXXXX
    """
    # UsernameBot = 'elpiBot'
    tokenBot = 'XXXXX'
    # bot_chatID = '1734506789'
    chatID = ['XXXXX', 'XXXXX']
    for i in chatID:
        # print(i, bot_message)
        send_text = 'https://api.telegram.org/bot' + tokenBot + '/sendMessage?chat_id=' + str(i) + '&parse_mode=Markdown&text=' + bot_message

        response = requests.get(send_text)
        # continue

    return response.json()
