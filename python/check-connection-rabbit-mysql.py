# Автор: Igor A. Shmakov
# Первая версия: 2021-XX-XX
# Версия от: 2021-11-13
# № версии: 0.05
# Лицензия: GPLv3

import os
from datetime import datetime, timedelta
import time

# Подключение локальных модулей
import sendMessageToTelegramBot as sMTTB
import getTimeFromDB as gTFDB
import convertDBString as cDBS
import sendMQTTAllert as sMQTTA


def checkTimeInDB(dbString, fDict):
    """
    Функция проверяет время внесения данных в базу, если время больше
    чем 10 минут, то выставляет флаг в словаре.
    """
    timeNOW = datetime.now()  # datetime.utcnow() # datetime.now()  # .strftime("%Y-%m-%d %H:%M:%S")

    for i in range(len(dbString)-1):
        string = dbString[i].split('\t')

        for j in range(len(string)):
            for msg in fDict:
                if (string[j] == msg):
                    timeINBASE = datetime.strptime(string[9], '%Y-%m-%d %H:%M:%S')

                    dMinutes = 10
                    timeWD = timeINBASE + timedelta(minutes=dMinutes)
                    if (timeNOW > timeWD):
                        print("Больше", timeNOW, timeWD)
                        fDict[msg][1] = fDict[msg][0]
                        fDict[msg][0] = True
                    else:
                        print("Меньше", timeNOW, timeINBASE, timeWD)
                        fDict[msg][1] = fDict[msg][0]
                        fDict[msg][0] = False
                else:
                    continue


if __name__ == "__main__":
    serverIP = '192.168.5.1'
    count = 0
    while True:
        #dbString = gTFDB.getTimeFromDB()  # Получение данных из базы
        dbString = ['id\tparent_id\tdevice_id\treliability_id\tmap_id\tout_of_service\tbacnet_name\tvalue\tstatus\ttimestamp\tObject_Identifier\tObject_Name\tObject_Type\tProperty_List\tLog_Enable', '193\t191\t11201\t0\t0\t0\tNULL\t1\t0\t2021-10-30 17:23:02\t31204\tSite:Manjerok/RVCH-02.lhight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '194\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31202\tSite:Manjerok/RVCH-02.llow\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '195\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31203\tSite:Manjerok/RVCH-02.lmed\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '196\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31207\tSite:Manjerok/RVCH-02.rhight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '197\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31205\tSite:Manjerok/RVCH-02.rlow\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '198\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31206\tSite:Manjerok/RVCH-02.rmed\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '201\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31210\tSite:Manjerok/RVCH-02.vacc\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '202\t191\t11201\t0\t0\t0\tNULL\t1\t0\t2021-10-30 17:23:00\t31211\tSite:Manjerok/RVCH-02.vmain\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '200\t191\t11201\t0\t0\t0\tNULL\t100\t0\t2021-10-29 20:46:33\t31209\tSite:Manjerok/RVCH-02.tout\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '184\t1\t1\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t12345\tSite:Manjerok\tfolder\t{"template":"/svg/manj-main.svg","alias":"","replace":{}}\t0', '185\t184\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t11001\tSite:Manjerok/RVCH-01\tdevice\t{"template":"/svg/manj01/manj01-main.svg","alias":"","replace":{}}\t0', '186\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31012\tSite:Manjerok/RVCH-01.alarm\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '187\t185\t11001\t0\t0\t0\tNULL\t100\t0\t2021-10-19 14:03:05\t31011\tSite:Manjerok/RVCH-01.depth\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '188\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31004\tSite:Manjerok/RVCH-01.hight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '189\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31002\tSite:Manjerok/RVCH-01.low\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '190\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31003\tSite:Manjerok/RVCH-01.med\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '191\t184\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t11201\tSite:Manjerok/RVCH-02\tdevice\t{"template":"/svg/manj02/manj02-main.svg","alias":"","replace":{}}\t0', '192\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31212\tSite:Manjerok/RVCH-02.alarm\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '199\t191\t11201\t0\t0\t0\tNULL\t100\t0\t2021-10-19 14:03:05\t31208\tSite:Manjerok/RVCH-02.tin\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '1\t0\tNULL\t0\t0\t0\tNULL\t0\t0\tNULL\t1\tSite\tsite\tNULL\t0', '']
        fState, fDict = cDBS.convertDBString(dbString, count)  # Получение данных о состоянии флагов

        checkTimeInDB(dbString, fDict)

        for key in fDict:
            if (fDict[key][0] is True):
                sMQTTA.sendMQTTAllert(serverIP, key, fDict) # нужно подумать как отправлять MQTT запрос.
                bot_message = 'Скрипт не получает обновление базы более 10 минут! ' + str(key[14:])
                # bot_message += str(key)
                # sMTTB.sendMessageToTelegramBot(bot_message)
                # print(bot_message)
                print("Значение флага отвечающего за отправку сообщения в", str(fDict[key]), "равно True!")
            elif (fDict[key][0] != fDict[key][1]):
                # print(fDict[key][0], fDict[key][1])
                sMQTTA.sendMQTTAllert(serverIP, key, fDict) # нужно подумать как отправлять MQTT запрос.
                bot_message = 'Изменение состояние флагов! ' + str(key[14:])
                sMTTB.sendMessageToTelegramBot(bot_message)
                print("Значение флага на прошлом и текущем шагу отличаются", str(fDict[key]))
            else:
                print("Значение флага отвечающего за отправку сообщения в", str(fDict[key]), "равно False!")
                continue

        count += 1
        print("Номер завершенной итеррации №", count)
        time.sleep(300)
