# Автор: Igor A. Shmakov
# Первая версия: 2021-XX-XX
# Версия от: 2021-11-13
# № версии: 0.06
# Лицензия: GPLv3

def convertDBString(sMySQL, count):
    """
    11-й столбец имена.
    Site:Manjerok/RVCH-02.lhight
    Site:Manjerok/RVCH-02.lmed
    Site:Manjerok/RVCH-02.llow
    Site:Manjerok/RVCH-02.rhight
    Site:Manjerok/RVCH-02.rlow
    Site:Manjerok/RVCH-02.rmed
    6-ой столбец состояние.
    1 - True - есть сигнал.
    0 - False - сигнала нет.
    Parameters:
            sMySQL (list): ответ из базы:

        Returns:
            fListState (bool): список сигнало.
            fDictState (dict): ключ: ['флаг состояние отправки сообщения по сигналу'
            'состояние флага на прошлом шагу отправки сообщения по сигналу', 'флаг сигнала',
            'идентификатор устройства', 'идентификатор датчика', 'идентификатор сигнала alarm']
    """
    fListState = [False, False, False, False, False, False]
    if count == 0:
        fDictState = {'Site:Manjerok/RVCH-01.hight': [False, False, False, 0, 0, 0],
                      'Site:Manjerok/RVCH-01.med': [False, False, False, 0, 0, 0],
                      'Site:Manjerok/RVCH-01.low': [False, False, False, 0, 0, 0],
                      'Site:Manjerok/RVCH-02.lhight': [False, False, False, 0, 0, 0],
                      'Site:Manjerok/RVCH-02.lmed': [False, False, False, 0, 0, 0],
                      'Site:Manjerok/RVCH-02.llow': [False, False, False, 0, 0, 0],
                      'Site:Manjerok/RVCH-02.rhight': [False, False, False, 0, 0, 0],
                      'Site:Manjerok/RVCH-02.rlow': [False, False, False, 0, 0, 0],
                      'Site:Manjerok/RVCH-02.rmed': [False, False, False, 0, 0, 0]}
    else:
        print(count)
        # continue

    for i in range(len(sMySQL) - 1):
        string = sMySQL[i].split('\t')

        for j in range(len(string)):
            for msg in fDictState:
                if (string[j] == msg):
                    # print(j, msg, string[j-4])
                    if (string[j - 4] == '1'):
                        # print('True', string[j-4])
                        fDictState[msg][2] = True
                    elif (string[j - 4] == '0'):
                        fDictState[msg][2] = False
                    else:
                        print('Ошибка в базе!')
                    
                    fDictState[msg][3] = string[j - 9]
                    fDictState[msg][4] = string[j - 1]
                elif ('alarm' in string[j]):
                    tempString = string[j]
                    tempString = tempString.split('/')
                    tempString = tempString[1].split('.')
                    tempString = tempString[0]

                    for i in fDictState:
                        if (tempString in i):
                            fDictState[i][5] = string[j - 1]
                else:
                    continue

    fListState[2] = fDictState['Site:Manjerok/RVCH-02.lhight'][2]
    fListState[1] = fDictState['Site:Manjerok/RVCH-02.lmed'][2]
    fListState[0] = fDictState['Site:Manjerok/RVCH-02.llow'][2]
    fListState[5] = fDictState['Site:Manjerok/RVCH-02.rhight'][2]
    fListState[4] = fDictState['Site:Manjerok/RVCH-02.rmed'][2]
    fListState[3] = fDictState['Site:Manjerok/RVCH-02.rlow'][2]

    return fListState, fDictState
