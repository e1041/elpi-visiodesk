# Автор: Igor A. Shmakov
# Первая версия: 2021-XX-XX
# Версия от: 2021-11-13
# № версии: 0.05
# Лицензия: GPLv3

import os


def getTimeFromDB():
    """
    Модуль получает данные из базы через вызов скрипта в docker контейнере.

        Parameters:
            : не требует на вход переменных.

        Returns:
            resultArray (str): ответ от базы.

    # dockerMySQL = "docker exec -it master sh -c 'mysql -u root -pXXXXX vbas -e "SELECT * FROM objects ORDER BY timestamp DESC;"'"
    # resultMySQL = 'id\tparent_id\tdevice_id\treliability_id\tmap_id\tout_of_service\tbacnet_name\tvalue\tstatus\ttimestamp\tObject_Identifier\tObject_Name\tObject_Type\tProperty_List\tLog_Enable\n47\t46\t11001\t0\t0\t0\tNULL\t0\t0\t2021-07-26 07:46:31\t31010\tSite:Manjerok/Amtek_RCHV.Vol1.depth\tanalog-input\t{"template":"","alias":"","replace":{}}\t0\n48\t46\t11001\t0\t0\t0\tNULL\t0\t0\t2021-07-26 07:25:51\t31004\tSite:Manjerok/Amtek_RCHV.Vol1.hight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0\n50\t46\t11001\t0\t0\t0\tNULL\t0\t0\t2021-07-26 07:25:51\t31003\tSite:Manjerok/Amtek_RCHV.Vol1.med\tbinary-input\t{"template":"","alias":"","replace":{}}\t0\n49\t46\t11001\t0\t0\t0\tNULL\t1\t0\t2021-07-26 07:25:50\t31002\tSite:Manjerok/Amtek_RCHV.Vol1.low\tbinary-input\t{"template":"","alias":"","replace":{}}\t0\n44\t1\t1\t0\t0\t0\tNULL\t0\t0\t2021-07-25 14:22:35\t5716\tSite:Manjerok\tfolder\t{"template":"/svg/MANJ_MAIN.svg","alias":"","replace":{}}\t0\n45\t44\t1\t0\t0\t0\tNULL\t0\t0\t2021-07-25 14:22:35\t31000\tSite:Manjerok/Amtek_RCHV\tfolder\t{"template":"/svg/MANJ_PODL.svg","alias":"","replace":{}}\t0\n46\t45\t1\t0\t0\t0\tNULL\t0\t0\t2021-07-25 14:22:35\t31001\tSite:Manjerok/Amtek_RCHV.Vol1\tfolder\t{"template":"","alias":"","replace":{}}\t0\n51\t44\t1\t0\t0\t0\tNULL\t0\t0\t2021-07-25 14:22:35\t2000\tSite:Manjerok/Amtek_dev\tfolder\t{"template":"","alias":"","replace":{}}\t0\n52\t51\t11001\t0\t0\t0\tNULL\t0\t0\t2021-07-25 14:22:35\t11001\tSite:Manjerok/Amtek_dev.RCHV\tdevice\t{"template":"","alias":"","replace":{}}\t0\n1\t0\tNULL\t0\t0\t0\tNULL\t0\t0\tNULL\t1\tSite\tsite\tNULL\t0\n'

    FIXME: XXXXX
    """
    dockerMySQL = 'docker exec -it master sh -c "mysql -u root -pXXXXX vbas < /opt/db-update-check.sql"'
    # os.system(dockerMySQL)
    resultMySQL = os.popen(dockerMySQL).read()
    resultArray = resultMySQL.split('\n')

    return resultArray
