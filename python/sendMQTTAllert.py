# Автор: Igor A. Shmakov
# Первая версия: 2021-XX-XX
# Версия от: 2021-11-13
# № версии: 0.05
# Лицензия: GPLv3

import os


def sendMQTTAllert(serverIP, keys, fDict):
    """
    Отправка запроса об ошибке визуализации.
    FIXME: XXX.XXX.XXX.XXX
    """
    print(serverIP, keys, fDict)
    msg = 'mosquitto_pub -h ' + str(serverIP) + ' -p 1883 -u user -P user -t Site/' + str(fDict[keys][3]) + ' -m \"' + str(fDict[keys][3]) + ' 3 ' + str(fDict[keys][4]) + ' ' + str(int(fDict[keys][0])) + ' 0"'

    # if timeFLAG != 0:
    #    mess_alert = 'mosquitto_pub -h XXX.XXX.XXX.XXX -p 1883 -u user -P user -t Site/11001 -m "11001 3 31010 1 0"'
    # else:
    #    mess_alert = 'mosquitto_pub -h XXX.XXX.XXX.XXX -p 1883 -u user -P user -t Site/11001 -m "11001 3 31010 0 0"'
    #print(f"MQTT запрос: {mess_alert}")
    print("MQTT запрос: ", msg)

    #os.system(msg)
