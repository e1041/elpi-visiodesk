# Автор: Igor A. Shmakov
# Первая версия: 2021-XX-XX
# Версия от: 2021-11-13
# № версии: 0.05
# Лицензия: GPLv3

import os
from datetime import datetime, timedelta
import time

# Подключение локальных модулей
import sendMessageToTelegramBot as sMTT
import getTimeFromDB as gTF
import convertDBString as cDBS


def testSignal(sState):
    """
    Модуль проверяет наличие ошибки состояния системы Манжерок РВЧ-02 из
    базы данных.

        Parameters:
            sState (list): список сигналов:
                h1 - высокий уровень (190) первой ёмкости;
                m1 - средний уровень (70) первой ёмкости;
                l1 - низкий уровень (50) первой ёмкости;
                h2 - высокий уровень (180) второй ёмкости;
                m2 - средний уровень (50) второй ёмкости;
                l2 - низкий уровень (40) второй ёмкости.

        Returns:
            signalStatus (bool): ответ о наличии ошибки.
    """
    h1 = sState[2]
    m1 = sState[1]
    l1 = sState[0]
    h2 = sState[5]
    m2 = sState[4]
    l2 = sState[3]

    if ((h1 and not m1) or (m2 and not l2) or (h2 and not m2) or (m1 and not l1) or (not m1 and h2) or (h1 and not m2) or (m1 and not m2 and not l2) or (not m1 and not l1 and m2)):
        sStatus = True  # Ошибка!
    else:
        sStatus = False  # Ошибки нет!

    return sStatus


if __name__ == "__main__":
    """
    signal = [False for i in range(0, 6)]

    h1 = signal[0]
    m1 = signal[1]
    l1 = signal[2]
    h2 = True
    m2 = True
    l2 = signal[5]

    signal[3] = h2
    signal[4] = m2

    print(testSignal(signal))
    """
    print("Запуск проверки состояния системы Манжерок РВЧ-02.")
    count = 0
    errorCount = 0

    while True:
        # dbString = gTF.getTimeFromDB()
        # manj02
        # dbString = ['id\tparent_id\tdevice_id\treliability_id\tmap_id\tout_of_service\tbacnet_name\tvalue\tstatus\ttimestamp\tObject_Identifier\tObject_Name\tObject_Type\tProperty_List\tLog_Enable', '193\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31204\tSite:Manjerok/RVCH-02.lhight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '194\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31202\tSite:Manjerok/RVCH-02.llow\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '195\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31203\tSite:Manjerok/RVCH-02.lmed\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '196\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31207\tSite:Manjerok/RVCH-02.rhight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '197\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31205\tSite:Manjerok/RVCH-02.rlow\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '198\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31206\tSite:Manjerok/RVCH-02.rmed\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '201\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31210\tSite:Manjerok/RVCH-02.vacc\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '202\t191\t11201\t0\t0\t0\tNULL\t1\t0\t2021-10-30 17:23:00\t31211\tSite:Manjerok/RVCH-02.vmain\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '200\t191\t11201\t0\t0\t0\tNULL\t100\t0\t2021-10-29 20:46:33\t31209\tSite:Manjerok/RVCH-02.tout\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '184\t1\t1\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t12345\tSite:Manjerok\tfolder\t{"template":"/svg/manj-main.svg","alias":"","replace":{}}\t0', '185\t184\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t11001\tSite:Manjerok/RVCH-01\tdevice\t{"template":"/svg/manj01/manj01-main.svg","alias":"","replace":{}}\t0', '186\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31012\tSite:Manjerok/RVCH-01.alarm\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '187\t185\t11001\t0\t0\t0\tNULL\t100\t0\t2021-10-19 14:03:05\t31011\tSite:Manjerok/RVCH-01.depth\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '188\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31004\tSite:Manjerok/RVCH-01.hight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '189\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31002\tSite:Manjerok/RVCH-01.low\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '190\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31003\tSite:Manjerok/RVCH-01.med\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '191\t184\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t11201\tSite:Manjerok/RVCH-02\tdevice\t{"template":"/svg/manj02/manj02-main.svg","alias":"","replace":{}}\t0', '192\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31212\tSite:Manjerok/RVCH-02.alarm\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '199\t191\t11201\t0\t0\t0\tNULL\t100\t0\t2021-10-19 14:03:05\t31208\tSite:Manjerok/RVCH-02.tin\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '1\t0\tNULL\t0\t0\t0\tNULL\t0\t0\tNULL\t1\tSite\tsite\tNULL\t0', '']
        dbString = ['id\tparent_id\tdevice_id\treliability_id\tmap_id\tout_of_service\tbacnet_name\tvalue\tstatus\ttimestamp\tObject_Identifier\tObject_Name\tObject_Type\tProperty_List\tLog_Enable', '193\t191\t11201\t0\t0\t0\tNULL\t1\t0\t2021-10-30 17:23:02\t31204\tSite:Manjerok/RVCH-02.lhight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '194\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31202\tSite:Manjerok/RVCH-02.llow\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '195\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31203\tSite:Manjerok/RVCH-02.lmed\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '196\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31207\tSite:Manjerok/RVCH-02.rhight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '197\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31205\tSite:Manjerok/RVCH-02.rlow\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '198\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31206\tSite:Manjerok/RVCH-02.rmed\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '201\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-30 17:23:02\t31210\tSite:Manjerok/RVCH-02.vacc\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '202\t191\t11201\t0\t0\t0\tNULL\t1\t0\t2021-10-30 17:23:00\t31211\tSite:Manjerok/RVCH-02.vmain\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '200\t191\t11201\t0\t0\t0\tNULL\t100\t0\t2021-10-29 20:46:33\t31209\tSite:Manjerok/RVCH-02.tout\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '184\t1\t1\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t12345\tSite:Manjerok\tfolder\t{"template":"/svg/manj-main.svg","alias":"","replace":{}}\t0', '185\t184\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t11001\tSite:Manjerok/RVCH-01\tdevice\t{"template":"/svg/manj01/manj01-main.svg","alias":"","replace":{}}\t0', '186\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31012\tSite:Manjerok/RVCH-01.alarm\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '187\t185\t11001\t0\t0\t0\tNULL\t100\t0\t2021-10-19 14:03:05\t31011\tSite:Manjerok/RVCH-01.depth\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '188\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31004\tSite:Manjerok/RVCH-01.hight\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '189\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31002\tSite:Manjerok/RVCH-01.low\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '190\t185\t11001\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31003\tSite:Manjerok/RVCH-01.med\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '191\t184\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t11201\tSite:Manjerok/RVCH-02\tdevice\t{"template":"/svg/manj02/manj02-main.svg","alias":"","replace":{}}\t0', '192\t191\t11201\t0\t0\t0\tNULL\t0\t0\t2021-10-19 14:03:05\t31212\tSite:Manjerok/RVCH-02.alarm\tbinary-input\t{"template":"","alias":"","replace":{}}\t0', '199\t191\t11201\t0\t0\t0\tNULL\t100\t0\t2021-10-19 14:03:05\t31208\tSite:Manjerok/RVCH-02.tin\tanalog-input\t{"template":"","alias":"","replace":{}}\t0', '1\t0\tNULL\t0\t0\t0\tNULL\t0\t0\tNULL\t1\tSite\tsite\tNULL\t0', '']
        #  print(dbString)
        rListFlag, stateDict = cDBS(dbString)
        fBoolState = testSignal(rListFlag)

        if (fBoolState is True):
            count = 0
            errorCount += 1
            sString = str(errorCount) + ' ' + str(rListFlag)
            # sMTT.sendMessageToTelegramBot(sString)
            print("Отправка сообщения об ошибке!", sString)
        elif (fBoolState is False):
            errorCount = 0
            count += 1
            sString = str(count) + ' ' + str(rListFlag)
            print("Ошибки нет!", sString)
            # continue
        else:
            print("Странное поведение системы!")
            break

        time.sleep(300)
