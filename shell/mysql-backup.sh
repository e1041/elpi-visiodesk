#!/bin/bash

set -e
set -x

SQLFROM=/opt/services/conf/mxs/sql/init-files/backup
SQLTO=/root/sql-backup

docker exec -i master sh -c "bash /opt/backup/backup-base.sh"

mv $SQLFROM/*.gz $SQLTO/

#ipv4  ipv6 FIXME: 
#XXXXX
#rsync -avuP $SQLTO/*.gz XXXXX@[XXXXX]:sql-backup/
